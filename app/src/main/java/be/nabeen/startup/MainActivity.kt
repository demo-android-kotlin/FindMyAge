package be.nabeen.findmyage

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    protected fun buFindAgeEvent(view:View) {
        val userDOB:String = etDOB.text.toString()
        val year:Int = Calendar.getInstance().get(Calendar.YEAR)

        when {
            validInput(txt = userDOB, year = year) -> {
                val myAge = year - userDOB.toInt()
                tvShowAge.text = "Your Age is $myAge years"
            }
            else -> {
                tvShowAge.text = "Invalid input"
            }
        }
    }

    private fun validInput(txt:String, year:Int):Boolean {
        return try {
            when {
                txt.toInt() == 0 || txt.toInt() > year -> {
                    false
                }
                else -> {
                    true
                }
            }
        }
        catch (e: NumberFormatException) {
            false
        }
    }
}
